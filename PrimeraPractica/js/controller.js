MVC.Controller = class Controller{
  constructor(props){
    // Escucha los eventos antes de crear el modelo que envía la petición
    this.eventHandler();
    this.model = new props.model(props.endpoint);
    this.view = new props.view(props.contentElement);
  }

  eventHandler(){
    document.body.addEventListener('onloadApp',(event)=>{
      this.getData();
    });
  }

  getData(){
    this.model.getPersona()
    .then(data=>{
      this.view.notify(data);
    })
    .catch(console.log);
  }
}

MVC.controllerInstance = new MVC.Controller({
  model:MVC.Model,
  view:MVC.View,
  contentElement:document.querySelector('#contact-form'),
  endpoint:'./model/persona.json'
});
document.body.dispatchEvent(new Event('onloadApp'));
// console.log(MVC.controllerInstance.model.getPersona());
