MVC.View = class View{
  constructor(elem){
    // Escucha primero los eventos
    this.eventHandler();

    this.elem = elem;
  }

  eventHandler(){
    document.body.addEventListener('onLoadData', (event)=>{
      this.updateView(event.detail);
    });
  }

  notify(data){
    const onLoadDataEvent = new CustomEvent('onLoadData',{
      detail:data, bubbles:true
    });
    this.elem.dispatchEvent(onLoadDataEvent);
  }

  updateView(datos){
    for(let key in datos){
      if(key == "sexo"){
        let radiobtns = document.getElementsByClassName("sexo");
        for(let radio of radiobtns){
          if(radio.value == datos[key]){
            radio.checked = true;
            break;
          }
        }
      }else{
        if(key == "trabajo"){
          for(let keytrabajo in datos.trabajo){
            this.writeData(datos.trabajo, keytrabajo);
          }
        }else{
          this.writeData(datos, key);
        }
      }
    }
  }

  writeData(data, key){
    const node = this.elem.querySelector(`#${key}`);
    if(node != null){
      node.value=data[key];
    }else{
      console.log(`No se encontró ${key}`);
    }
  }
}
